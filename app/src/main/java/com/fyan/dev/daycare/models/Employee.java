package com.fyan.dev.daycare.models;

import com.orm.SugarRecord;

public class Employee extends SugarRecord {
    String nrp;
    String nama;
    String alamat;

    public Employee() {
    }

    public Employee(String nrp, String nama, String alamat) {
        this.nrp = nrp;
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
