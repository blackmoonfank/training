package com.fyan.dev.daycare;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.fyan.dev.daycare.adapter.AdapterEmployee;
import com.fyan.dev.daycare.models.Employee;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnInput;
    RecyclerView rv_employee;
    AdapterEmployee adapterEmployee;
    LinearLayoutManager layoutManager;
    List<Employee> litEmployee;
//    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnInput =(Button) findViewById(R.id.btn_inpuform);
        rv_employee = (RecyclerView) findViewById(R.id.rv_employee);

//        layoutManager = new LinearLayoutManager(this,
//                LinearLayoutManager.HORIZONTAL,true);
//        gridLayoutManager = new GridLayoutManager(this,3);
        litEmployee = Employee.listAll(Employee.class);
        //List<Employee> litEmployee = getListEmployee();
        layoutManager = new LinearLayoutManager(this);
        rv_employee.setLayoutManager(layoutManager);
        adapterEmployee = new AdapterEmployee(this,litEmployee);
        rv_employee.setAdapter(adapterEmployee);

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),InputDataActivity.class);
                startActivity(intent);
            }
        });
    }

//    List<Employee> getListEmployee(){
//        List<Employee> iList = new ArrayList<>();
//        Employee emp = new Employee();
//        emp.setNrp("6212090");
//        emp.setNama("Supardi");
//        emp.setAlamat("Jakarta");
//        iList.add(emp);
//        return  iList;
//    }

    void refreshData(){
        litEmployee.clear();
        List<Employee> list = Employee.listAll(Employee.class);
        litEmployee.addAll(list);
        adapterEmployee.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public  void ConfirmDelete(final String nrp){
        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda yakin ?")
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Employee employee = Select.from(Employee.class).
                                where("NRP = ?",
                                new String[]{nrp}).first();
                        employee.delete();
                        refreshData();
                    }
                })
                .setNeutralButton("TIDAK",null)
                .show();
    }
}
