package com.fyan.dev.daycare;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fyan.dev.daycare.models.Employee;
import com.orm.query.Select;

import java.util.List;

public class InputDataActivity extends AppCompatActivity {
    EditText txtNrp, txtNama, txtAlamat;
    Button btnSimpan;
    boolean modeEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);

        modeEdit = getIntent().getBooleanExtra("modeEdit",false);
        String nrp = getIntent().getStringExtra("nrp");
        String nama = getIntent().getStringExtra("nama");
        String alamat = getIntent().getStringExtra("alamat");

        txtNrp = (EditText) findViewById(R.id.txtNrp);
        txtNama = (EditText) findViewById(R.id.txtNama);
        txtAlamat = (EditText) findViewById(R.id.txtAlamat);
        btnSimpan = (Button) findViewById(R.id.btnSave);

        if (modeEdit){
           txtNrp.setText(nrp);
           txtNama.setText(nama);
           txtAlamat.setText(alamat);
           txtNrp.setEnabled(false);
        }

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modeEdit){
                    btnEditClick();
                }
                else {
                    btnSimpanClick();
                }
            }
        });
    }

    void btnSimpanClick() {
        try {
            Employee book = new Employee(
                    txtNrp.getText().toString(),
                    txtNama.getText().toString(),
                    txtAlamat.getText().toString());
            book.save();

//            Employee employee = new Employee();
//            employee.setNrp(txtNrp.getText().toString());
//            employee.setNama(txtNama.getText().toString());
//            employee.save();

            clearData();
            finish();
//            getList();
        }catch (Exception ex){
            Log.d("guwe error",ex.toString());
        }
    }

    void btnEditClick() {
        try {
//            Employee book = Employee.find(Employee.class,
//                    "NRP = ?",
//                    new String[]{txtNrp.getText().toString()}).get(0);

            Employee employee = Select.from(Employee.class).where("NRP = ?",
                    new String[]{txtNrp.getText().toString()}).first();

            if (employee != null){
                employee.setNama(txtNama.getText().toString());
                employee.setAlamat(txtAlamat.getText().toString());
                employee.save();
                clearData();
                finish();
            }
            else {
                Toast.makeText(this, "Data Kosong", Toast.LENGTH_SHORT).show();
            }


//            getList();
        }catch (Exception ex){
            Log.d("guwe error",ex.toString());
        }
    }

    void getList(){
        List<Employee> Employees = Employee.listAll(Employee.class);
        Toast.makeText(getApplication(),String.valueOf(Employees.size()),
                Toast.LENGTH_LONG).show();
    }
    void clearData(){
        txtNrp.getText().clear();
        txtNama.getText().clear();
        txtAlamat.getText().clear();
    }
}
